import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TimesheetDetailsService } from '../services/timesheetDetails.service';
import { ActivatedRoute } from '@angular/router';
import { TaskService } from '../services/task.service';
import { TimeSheetDetailsAdd } from '../models/TimeSheetDetailsAdd.model';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'timesheetDetailsAdd',
    templateUrl: 'timesheetDetailsAdd.component.html',
    styleUrls:['./timesheetDetailsAdd.component.scss']
})

export class TimesheetDetailsAddComponent implements OnInit {
    tasks: any;
    selectedTask: any;
    selectedDate: Date;
    timeSheetDetailsAdd: TimeSheetDetailsAdd = new TimeSheetDetailsAdd();
    @Output() timesheetCreated = new EventEmitter();
    @Output() timesheetCanceled = new EventEmitter();
    @ViewChild('f') form: any;

    OnselectTaskChange() {
      this.timeSheetDetailsAdd.taskId = this.selectedTask.id;
    }

    constructor(private employeeService: EmployeeService,
                private timesheetDetailsService: TimesheetDetailsService,
                private taskService: TaskService,
                private route: ActivatedRoute,
                private toastr: ToastrService

    ) {
      this.timeSheetDetailsAdd = new TimeSheetDetailsAdd();
    }

    ngOnInit() {
      this.taskService.getallTasks().subscribe(data => {
        this.tasks = data;
      },
        error => {
          this.toastr.error('oops something went wrong');
        });

      this.route.params.subscribe( params => {
        if (params['employeeId']) {
          this.timeSheetDetailsAdd.employeeId = params['employeeId'];
        }
      });
    }

    addTimesheet() {
      this.timeSheetDetailsAdd.workDate = this.selectedDate.getFullYear() + '-' +
      (this.selectedDate.getMonth() + 1 ) + '-' +
      this.selectedDate.getDate();
       if (this.form.valid) {
          this.timesheetDetailsService.AddTimesheetDetails(this.timeSheetDetailsAdd).subscribe(res => {
            this.timesheetCreated.emit();
          },
          error => {
            this.toastr.error('oops something went wrong');
          },
          () => {
            console.log('inserted');
            this.toastr.success('New record created successfully');
          }
        );
          this.form.reset();
      }
     }

    cancel() {
      this.form.reset();
      this.timesheetCanceled.emit();
    }

}
