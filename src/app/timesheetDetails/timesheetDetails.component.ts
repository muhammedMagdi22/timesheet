import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TimesheetDetailsService } from '../services/timesheetDetails.service';
import { TimesheetDetailList } from '../models/timesheetDetails.model';
import { Router, ActivatedRoute } from '@angular/router';
import Utils from '../helpers/utils';
import { map } from 'rxjs/operators';
import { WorkDay } from '../models/WorkDay';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'timesheetDetails-list',
    templateUrl: 'timesheetDetails.component.html',
    styleUrls: ['./timesheetDetails.component.scss']
})

export class TimesheetDetailsListComponent implements OnInit {
    employees: any;
    employeeId: number;
    startDate: string;
    endDate: string;
    workDayDetailList: WorkDay[];
    selectedEmployee: any;
    timesheetDetailsList: TimesheetDetailList[];
    hideAdd = true;
    dateRange: string;


    constructor(private employeeService: EmployeeService,
                private timesheetDetailsService: TimesheetDetailsService,
                private activatedroute: ActivatedRoute,
                private route: Router,
                private toastr: ToastrService
    ) {
    }


    ngOnInit() {
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
            this.endDate = null;
            this.activatedroute.params.subscribe( params => {
              if (params['startDate']) {
                this.startDate = params['startDate'];
                this.endDate = params['endDate'];
              }
              if (params['employeeId'] && moment(this.startDate).isValid() &&
                  moment(this.endDate).isValid() &&
                  moment(this.endDate).isAfter(this.startDate)) {
                this.startDate = Utils.FirstDayOfWeek( new Date(this.startDate)).toISOString().slice(0, 10);
                this.endDate =  Utils.LastDayOfWeek( new Date(this.startDate)).toISOString().slice(0, 10);
                this.selectedEmployee = this.employees.find(x => x.id == params['employeeId']);
                this.employeeId = params['employeeId'];
                this.getTimesheetDetails(params['employeeId'], this.startDate, this.endDate);
              } else {
                this.route.navigate(['']);
              }
            });
        },
        error => {
          this.toastr.error('oops something went wrong');
        });
    }


    getDateRange() {
      return moment(this.startDate).format('D MMM') + '  -  ' + moment(this.endDate).format('D MMM');
    }

    selectEmployee() {
      this.route.navigate(['/timesheetDetails', this.selectedEmployee.id, this.startDate, this.endDate]);
    }

    getEffortWork(items, currentDay): number {
      let effort = 0;
      items.forEach(item => {
        if (item.workDate.slice(0, 10) === currentDay.date) {
           effort = effort + item.effortTime;
        }
      });
      return effort;
    }

  getTimesheetDetails(id, startDate, endDate) {
    this.timesheetDetailsList = new Array<TimesheetDetailList>();
    this.workDayDetailList = Utils.GetDatesList(new Date(this.startDate), new Date(this.endDate));
    this.timesheetDetailsService.getEmployeeTimesheetDetails(id, startDate, endDate).pipe(
      map(items => {
        items.forEach(item => {
          this.workDayDetailList.forEach(workDay => {
            workDay.totalEffort += this.getEffortWork(item.timesheetDetails, workDay);
          });

        });
        return items;
      }))
      .subscribe(data => {
        this.timesheetDetailsList = data;
      },
        error => {
          this.toastr.error('oops something went wrong');
        });
  }


    addTimesheet() {
      this.getTimesheetDetails(this.employeeId, this.startDate, this.endDate);
      this.hideAdd = true;
    }

    showAddForm() {
      this.hideAdd = false;
    }

    hideAddForm() {
      this.hideAdd = true;
    }

    getDayName(dateAsString) {
      return Utils.getDayName(dateAsString);
    }

    previousWeek() {
      // tslint:disable-next-line:prefer-const
       let dt = new Date(this.startDate);
       dt.setDate(dt.getDate() - 7);
       this.startDate = dt.toISOString().slice(0, 10);
       this.endDate = Utils.LastDayOfWeek(dt).toISOString().slice(0, 10);
      this.route.navigate(['/timesheetDetails', this.selectedEmployee.id, this.startDate, this.endDate]);

    }
    nextWeek() {
      // tslint:disable-next-line:prefer-const
      let dt = new Date(this.startDate);
      dt.setDate(dt.getDate() + 7);
      this.startDate = dt.toISOString().slice(0, 10);
      this.endDate = Utils.LastDayOfWeek(dt).toISOString().slice(0, 10);
      this.route.navigate(['/timesheetDetails', this.selectedEmployee.id, this.startDate, this.endDate]);
    }

}
