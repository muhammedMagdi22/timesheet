import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';
import Utils from '../helpers/utils';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    startDate: string;
    endDate: string;
    constructor(private employeeService: EmployeeService,
                private route: Router,
                private toastr: ToastrService

    ) { }

  ngOnInit() {
    let dt = new Date();
    dt.setHours(12, 0, 0, 0);
    this.startDate = Utils.FirstDayOfWeek(dt).toISOString().slice(0, 10);
    this.endDate = Utils.LastDayOfWeek(dt).toISOString().slice(0, 10);
    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
    },
      error => {
        console.log(error);
        this.toastr.error('oops something went wrong');
      });
  }
}
