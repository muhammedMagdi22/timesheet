import { WorkDay } from "../models/WorkDay";
import * as moment from 'moment';
const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

export default class Utils {
  static FirstDayOfWeek(val: Date): Date {
    // satrday
    const firstDay = 6;
      let diff = val.getDay() - firstDay;
      if (diff < 0) {
        diff += 7;
      }
      val.setDate(val.getDate() - diff);
      return val;
  }

  static LastDayOfWeek(val: Date): Date {
    // tslint:disable-next-line:prefer-const
    let dt = this.FirstDayOfWeek(val);
    dt.setDate(dt.getDate() + 6);
    return dt;
  }

 static GetDatesList(startDate, stopDate) {
   let dayformat = 'dddd';
   if (moment(stopDate).diff(moment(startDate), 'days') > 7) {
     dayformat = 'ddd';
   }
   // tslint:disable-next-line:prefer-const
    let dateArray = new Array<WorkDay>();
    // tslint:disable-next-line:prefer-const
    let currentDate = startDate;
    while (currentDate <= stopDate) {
      let dt = new Date (currentDate);
        dateArray.push(new WorkDay( dt.toISOString().slice(0, 10), moment(dt).format(dayformat).toString(), 0));
        currentDate.setDate(currentDate.getDate() + 1);
    }
    return dateArray;
}

static getDayName(dt: Date): string {
  return  weekdays[dt.getDay()];
}


}
