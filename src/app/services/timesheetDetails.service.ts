import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { TimeSheetDetailsAdd } from '../models/timesheetDetailsAdd.model';
import { Observable } from 'rxjs';
import { TimesheetDetailList } from '../models/timesheetDetails.model';

@Injectable()
export class TimesheetDetailsService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallTimesheetDetails() {
        return this.http.get(this.baseapi + '/TimeSheetDetails');
    }

    getEmployeeTimesheetDetails(employeeId, startDate, endDate): Observable<TimesheetDetailList[]> {
      let url = `${this.baseapi}/TimeSheetDetails/${employeeId}/${startDate}/${endDate}`;
      return this.http.get<TimesheetDetailList[]>(url);
  }

  AddTimesheetDetails(timeSheetDetailsItem: TimeSheetDetailsAdd) {
    let url = `${this.baseapi}/TimeSheetDetails`;
    return this.http.post(url, timeSheetDetailsItem);
  }
}
