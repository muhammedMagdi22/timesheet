import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetDetailsListComponent } from './timesheetDetails/timesheetDetails.component';
import { TimesheetDetailsService } from './services/timesheetDetails.service';
import {Routes, RouterModule} from '@angular/router';
import { TaskService } from './services/task.service';
import { TimesheetDetailsAddComponent } from './timesheetDetailsAdd/timesheetDetailsAdd.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes: Routes = [

  {path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: EmployeeListComponent },
  { path: 'timesheetDetails', component: TimesheetDetailsListComponent  },
  { path: 'timesheetDetails/:employeeId/:startDate/:endDate', component: TimesheetDetailsListComponent  },
  {path: '**', component: EmployeeListComponent}
 ];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetDetailsListComponent,
    TimesheetDetailsAddComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule.forRoot(routes, {useHash: false}),
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 10000,
    positionClass: 'toast-top-center',
    })
  ],
  providers: [
    EmployeeService,
    TimesheetDetailsService,
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
