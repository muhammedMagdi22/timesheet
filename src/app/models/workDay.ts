export class WorkDay {
  constructor(
    public date: string = '',
    public dayName: string = '',
    public totalEffort: number = 0
  ) {}
}
