
export class TimeSheetDetailsAdd {
  constructor(public taskId: number = 0,
  public employeeId: number = 0,
  public workDate: string = '',
  public effortTime: number = 0) {}
}
