
export class TimeSheetDetails {
  constructor(public saturday: number = 0,
  public sunday: number = 0,
  public monday: number = 0,
  public tuesday: number = 0,
  public wednesday: number = 0,
  public thursday: number = 0,
  public friday: number = 0,
  public taskName: string = '') {}
}


export class TimesheetDetailList {
constructor (public taskName: string = '',
             public timesheetDetails: TimesheetDetailItem[] = new Array<TimesheetDetailItem>()) {
            }
}


export class TimesheetDetailItem {
  constructor( public workDate: Date = null,
              public effortTime: number = 0) {
              }
}

